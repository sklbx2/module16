#include <iostream>
#include <ctime> // ��� time() � localtime_s()

// ������ ����������� ���� � ��������� tm
void setNowTime(tm* tm_struct)
{
    // �������� ����� � �������� � 01.01.1970
    time_t now = time(NULL);
    // �������������� ���� time_t � ��������� �� �������� tm
    localtime_s(tm_struct, &now);
}

int main()
{
    const int N = 5;
    int array[N][N];

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << ' ';
        }
        std::cout << std::endl;
    }

    tm nowtime;
    setNowTime(&nowtime);
    int row = nowtime.tm_mday % N;

    std::cout << "\nToday day of month is " << nowtime.tm_mday<<'\n';
    std::cout << "Array size is " << N << '\n';
    std::cout << "Row is " << row << '\n';

    std::cout << "Sum of elements in chosen row:\n";

    int sum{ 0 };
    for (int j = 0; j < N; j++)
    {
        sum += array[row][j];
    }
    std::cout << sum << std::endl;
}
